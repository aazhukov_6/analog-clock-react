import './App.css';
import { useEffect, useState } from 'react';
const App = () => {
  const [date, setDate] = useState(new Date())

  useEffect(() => {
    const timerId = setInterval(() => {
      setDate(new Date());
    }, 1000);

    const time = [date.getHours(), date.getMinutes(), date.getSeconds()];
    const [hours, minutes, seconds] = time;

    const degHour = hours * (360 / 12) + minutes * (360 / 12 / 60);
    const degMin = minutes * (360 / 60) + seconds * (360 / 60 / 60);
    const degSec = seconds * (360 / 60);

    const rootStyle = document.documentElement.style;
    rootStyle.setProperty("--degHour", `${degHour}deg`);
    rootStyle.setProperty("--degMin", `${degMin}deg`);
    rootStyle.setProperty("--degSec", `${degSec}deg`);

    return () => clearInterval(timerId);
  }, [date]);

  return(
    <div className='clock'>
      <div className="hours-hand"></div>
      <div className="minutes-hand"></div>
      <div className="seconds-hand"></div>

      <div className="number number1"><div>1</div></div>
      <div className="number number2"><div>2</div></div>
      <div className="number number3"><div>3</div></div>
      <div className="number number4"><div>4</div></div>
      <div className="number number5"><div>5</div></div>
      <div className="number number6"><div>6</div></div>
      <div className="number number7"><div>7</div></div>
      <div className="number number8"><div>8</div></div>
      <div className="number number9"><div>9</div></div>
      <div className="number number10"><div>10</div></div>
      <div className="number number11"><div>11</div></div>
      <div className="number number12"><div>12</div></div>
    </div>
  );
};

export default App;
